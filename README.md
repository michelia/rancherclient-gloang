Golang rest client for the rancher API


**Contact**

For bugs, questions, comments, corrections, suggestions, etc., open an issue in [rancher/rancher](//github.com/rancher/rancher/issues) with a title starting with `[rancherclient-gloang] `.

Or just [click here](//github.com/rancher/rancher/issues/new?title=%5Brancherclient-gloang%5D%20) to create a new issue.
